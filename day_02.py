puzzle_input = [1, 0, 0, 3, 1, 1, 2, 3, 1, 3, 4, 3, 1, 5, 0, 3, 2, 13, 1, 19, 1, 19, 10, 23, 1, 23, 13, 27, 1, 6, 27,
                31, 1, 9, 31, 35, 2, 10, 35, 39, 1, 39, 6, 43, 1, 6, 43, 47, 2, 13, 47, 51, 1, 51, 6, 55, 2, 6, 55, 59,
                2, 59, 6, 63, 2, 63, 13, 67, 1, 5, 67, 71, 2, 9, 71, 75, 1, 5, 75, 79, 1, 5, 79, 83, 1, 83, 6, 87, 1,
                87, 6, 91, 1, 91, 5, 95, 2, 10, 95, 99, 1, 5, 99, 103, 1, 10, 103, 107, 1, 107, 9, 111, 2, 111, 10, 115,
                1, 115, 9, 119, 1, 13, 119, 123, 1, 123, 9, 127, 1, 5, 127, 131, 2, 13, 131, 135, 1, 9, 135, 139, 1, 2,
                139, 143, 1, 13, 143, 0, 99, 2, 0, 14, 0]

# puzzle_input = [1, 0, 0, 0, 99]
# puzzle_input = [2, 3, 0, 3, 99]
# puzzle_input = [2, 4, 4, 5, 99, 0]
# puzzle_input = [1, 1, 1, 4, 99, 5, 6, 0, 99]


def add(a, b):
    return a + b


def multiply(a, b):
    return a * b


def intcode(inputs):
    pc = 0
    while True:
        opcode = inputs[pc]

        if opcode == 1:
            operation = add
        elif opcode == 2:
            operation = multiply
        elif opcode == 99:
            break
        else:
            raise ValueError(f'Wrong opcode: {opcode}, pc: {pc}')

        a = inputs[inputs[pc + 1]]
        b = inputs[inputs[pc + 2]]
        result = operation(a, b)
        inputs[inputs[pc + 3]] = result

        pc = pc + 4

    return inputs


if __name__ == '__main__':
    # part one
    memory = puzzle_input.copy()
    memory[1] = 12
    memory[2] = 2
    print(intcode(memory)[0])

    # part two
    for noun in range(0, 100):
        for verb in range(0, 100):
            memory = puzzle_input.copy()
            memory[1] = noun
            memory[2] = verb

            program_output = intcode(memory)[0]
            if program_output == 19690720:
                print('Found match:', program_output)
                print('noun: ', noun)
                print('verb: ', verb)
                print('answer: ', 100 * noun + verb)
                print()


