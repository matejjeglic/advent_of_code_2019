import math
import sys

if __name__ == '__main__':

    # read wire path from file
    filename = 'day_03.txt'
    wires_path = {}
    with open(filename) as fp:
        for cnt, line in enumerate(fp):
            wires_path[cnt] = [x.strip() for x in line.split(',')]

    print(wires_path[0])
    print(wires_path[1])

    # convert wire path to coordinates
    wires_coords = {}
    for k, wire_path in wires_path.items():
        x = 0
        y = 0
        wires_coords[k] = [(x, y)]
        for path in wire_path:
            direction = path[0]
            steps = path[1:]
            # print(direction, steps)
            for i in range(int(steps)):
                if direction == 'U':
                    y += 1
                elif direction == 'D':
                    y -= 1
                elif direction == 'L':
                    x -= 1
                elif direction == 'R':
                    x += 1
                else:
                    raise ValueError(f'Wrong direction: {direction}')

                wires_coords[k].append((x, y))

    # check for crossings by comparing the coordinates of the two wires
    print(len(wires_coords[0]))
    print(len(wires_coords[1]))

    coords_0 = set(wires_coords[0])
    coords_1 = set(wires_coords[1])
    crossings = coords_0.intersection(coords_1)
    print()
    print('crossings: ', crossings)
    print('number of crossings: ', len(crossings))

    # crossings = []
    # for i, coord in enumerate(wires_coords[0]):
    #     sys.stdout.write("\r{:1.2f} %".format(i / len(wires_coords[0]) * 100))
    #     sys.stdout.flush()
    #     if coord in wires_coords[1]:
    #         crossings.append(coord)
    #
    # print()
    # print('crossings: ', crossings)

    distances = []
    for crossing in crossings:
        distances.append(int(math.fabs(crossing[0]) + math.fabs(crossing[1])))

    distances.sort()
    print('distances: ', distances)
    print('answer: ', distances[1])

    # second part of the puzzle
    crossings_steps = {}
    for k, wire_coords in wires_coords.items():
        crossings_steps[k] = [0] * len(crossings)
        for steps, coord in enumerate(wire_coords):
            for i, crossing in enumerate(crossings):
                if coord == crossing and crossings_steps[k][i] == 0:
                    crossings_steps[k][i] = steps

    print(crossings_steps[0])
    print(crossings_steps[1])

    wires_steps = []
    for steps_0, steps_1 in zip(crossings_steps[0], crossings_steps[1]):
        wires_steps.append(steps_0 + steps_1)

    wires_steps.sort()
    print('steps: ', wires_steps)
    print('answer: ', wires_steps[1])


