range_min = 387638
range_max = 919123

if __name__ == '__main__':

    # first part of the puzzle
    number_possible_passwords = 0
    for password in range(range_min, range_max + 1):
        digits = [int(digit) for digit in str(password)]
        # print(digits)

        # First condition: Two adjacent digits are the same (like 22 in 122345).
        first_condition = len(set(digits)) < len(digits)

        # Second condition: Going from left to right, the digits never decrease;
        # they only ever increase or stay the same (like 111123 or 135679).
        digits_sorted = digits.copy()
        digits_sorted.sort()
        second_condition = (digits_sorted == digits)

        if first_condition and second_condition:
            number_possible_passwords += 1

    print('number possible_passwords: ', number_possible_passwords)

    # second part of the puzzle
    number_possible_passwords = 0
    for password in range(range_min, range_max + 1):
        digits = [int(digit) for digit in str(password)]
        # print(digits)

        # First condition: Two adjacent digits are the same (like 22 in 122345).
        digits_set = set(digits)
        repeats = [0] * len(digits_set)
        first_condition = True
        for i, d in enumerate(digits_set):
            repeats[i] = 0
            for digit in digits:
                if d == digit:
                    repeats[i] += 1

        cnt = 0
        for repeat in repeats:
            if repeat == 2:
                cnt += 1

        if cnt < 1:
            first_condition = False

        # Second condition: Going from left to right, the digits never decrease;
        # they only ever increase or stay the same (like 111123 or 135679).
        digits_sorted = digits.copy()
        digits_sorted.sort()
        second_condition = (digits_sorted == digits)

        if first_condition and second_condition:
            number_possible_passwords += 1

    print('number possible_passwords: ', number_possible_passwords)
